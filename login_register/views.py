from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages


# Create your views here.
def home(request):
    if ('username' in request.session.keys()):
        response={}
        response['role']=request.session['role']
        print (response['role'])
        if ('nama' in request.session.keys()):
            response['nama']=request.session['nama']
        response['username']=request.session['username']
        return render(request, 'home.html',response)
    return HttpResponseRedirect(reverse('account:login'))

def login(request):
    if ('username' in request.session.keys()):
        return HttpResponseRedirect(reverse('home'))
    return render(request, 'login.html')

def post_login(request):
    username = request.POST['username']
    password = request.POST['password']
    cursor=connection.cursor()
    cursor.execute("SELECT * from pengguna where username='"+username+"'")
    hasil=cursor.fetchone();
    if (hasil):
        cursor.execute("SELECT password from pengguna where username='"+username+"'")
        hasil=cursor.fetchone();
        if (hasil[0]==password):
            request.session['username']=username
            cursor.execute("SELECT role from pengguna where username='"+username+"'")
            hasil=cursor.fetchone();
            request.session['role'] = hasil[0]
            if (hasil[0]!="admin"):
                role=hasil[0]
                if (role!="mahasiswa"):
                    role="donatur"
                cursor.execute("SELECT nama from " +role+ " where username='"+username+"'")
                hasil=cursor.fetchone();
                request.session['nama']=hasil[0]

            return HttpResponseRedirect(reverse('home'))
    messages.error(request, "Username atau password salah")
    return HttpResponseRedirect(reverse('account:login'))


def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('account:login'))

def register(request):
    if ('username' in request.session.keys()):
        return HttpResponseRedirect(reverse('home'))
    return render(request, 'register.html')

def register_mahasiswa(request):
    if ('username' in request.session.keys()):
        return HttpResponseRedirect(reverse('home'))
    return render(request, 'register_mahasiswa.html')

def register_donatur(request):
    if ('username' in request.session.keys()):
        return HttpResponseRedirect(reverse('home'))
    return render(request, 'register_donatur.html')

def post_mahasiswa(request):
    username = request.POST['username']
    password = request.POST['password']
    npm  = request.POST['npm']
    email = request.POST['email']
    nama = request.POST['nama_lengkap']
    nomor_telepon = request.POST['nomor_telepon']
    alamat = request.POST['alamat']
    alamat_domi = request.POST['alamat_domi']
    bank = request.POST['bank']
    alamat = request.POST['alamat']
    rekening = request.POST['rekening']
    nama_pemilik = request.POST['nama_pemilik']


    cursor=connection.cursor()
    cursor.execute("SELECT * from pengguna where username='"+username+"'")
    hasil=cursor.fetchone();
    if (hasil):
        if len(hasil)>0:
            messages.error(request, "Register Gagal: Username "+username+" sudah pernah diregistrasi")
            return HttpResponseRedirect(reverse('account:register-mahasiswa'))

    cursor=connection.cursor()
    cursor.execute("SELECT * from mahasiswa where npm='"+npm+"'")
    hasil=cursor.fetchone();
    if (hasil):
        if len(hasil)>0:
            messages.error(request, "Register Gagal: NPM "+npm+" sudah pernah diregistrasi")
            return HttpResponseRedirect(reverse('account:register-mahasiswa'))


    values = "'"+username+"','"+password+"','mahasiswa'"
    query = "INSERT INTO pengguna (username,password,role) values (" + values + ")"
    cursor.execute(query)

    values = "'"+npm+"','"+email+"','"+nama+"','"+nomor_telepon+"','"+alamat+"','"+alamat_domi+"','"+bank+"','"+rekening+"','"+nama_pemilik+"','"+username+"'"
    query = "INSERT INTO mahasiswa (npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) values (" + values + ")"
    cursor.execute(query)

    request.session['username']=username
    request.session['nama']=nama
    request.session['role']='mahasiswa'

    messages.success(request, "Register Berhasil: "+username)
    return HttpResponseRedirect(reverse('home'))

def post_individual_donor(request):
    username = request.POST['username_individual']
    password = request.POST['password_individual']
    ni  = request.POST['ni_individual']
    nik = request.POST['nik']
    email = request.POST['email_individual']
    nama = request.POST['nama_lengkap_individual']
    npwp = request.POST['npwp']
    nomor_telepon = request.POST['nomor_telepon_individual']
    alamat = request.POST['alamat_indivual']

    cursor=connection.cursor()
    cursor.execute("SELECT * from pengguna where username='"+username+"'")
    hasil=cursor.fetchone();
    if (hasil):
        if len(hasil)>0:
            messages.error(request, "Register Gagal: Username "+username+" sudah pernah diregistrasi")
            return HttpResponseRedirect(reverse('account:register-mahasiswa'))

    cursor=connection.cursor()
    cursor.execute("SELECT * from donatur where nomor_identitas = '"+ni+"'")
    hasil=cursor.fetchone();
    if (hasil):
        if len(hasil)>0:
            messages.error(request, "Register Gagal: Nomor Identitas "+ni+" sudah pernah diregistrasi")
            return HttpResponseRedirect(reverse('account:register-mahasiswa'))

    cursor=connection.cursor()
    cursor.execute("SELECT * from individual_donor where nik='"+nik+"'")
    hasil=cursor.fetchone();
    if (hasil):
        if len(hasil)>0:
            messages.error(request, "Register Gagal: NIK "+nik+" sudah pernah diregistrasi")
            return HttpResponseRedirect(reverse('account:register-mahasiswa'))

    values = "'"+username+"','"+password+"','individual_donor'"
    query = "INSERT INTO pengguna (username,password,role) values (" + values + ")"
    cursor.execute(query)

    values = "'"+ni+"','"+email+"','"+nama+"','"+npwp+"','"+nomor_telepon+"','"+alamat+"','"+username+"'"
    query = "INSERT INTO donatur (nomor_identitas,email,nama,npwp,no_telp,alamat,username) values (" + values + ")"
    cursor.execute(query)

    values = "'"+nik+"','"+ni+"'"
    query = "INSERT INTO individual_donor (nik,nomor_identitas_donatur) values (" + values + ")"
    cursor.execute(query)

    request.session['username']=username
    request.session['nama']=nama
    request.session['role']='individual_donor'

    messages.success(request, "Register Berhasil: "+username)
    return HttpResponseRedirect(reverse('home'))

def post_yayasan(request):
    username = request.POST['username_yayasan']
    password = request.POST['password_yayasan']
    ni  = request.POST['ni_yayasan']
    nsk = request.POST['nsk']
    email = request.POST['email_yayasan']
    nama = request.POST['nama_yayasan']
    nomor_telepon = request.POST['nomor_telepon_yayasan']
    npwp = request.POST['npwp_yayasan']
    alamat = request.POST['alamat_yayasan']

    cursor=connection.cursor()
    cursor.execute("SELECT * from pengguna where username='"+username+"'")
    hasil=cursor.fetchone();
    if (hasil):
        if len(hasil)>0:
            messages.error(request, "Register Gagal: Username "+username+" sudah pernah diregistrasi")
            return HttpResponseRedirect(reverse('account:register-mahasiswa'))

    cursor=connection.cursor()
    cursor.execute("SELECT * from donatur where nomor_identitas = '"+ni+"'")
    hasil=cursor.fetchone();
    if (hasil):
        if len(hasil)>0:
            messages.error(request, "Register Gagal: Nomor Identitas "+ni+" sudah pernah diregistrasi")
            return HttpResponseRedirect(reverse('account:register-mahasiswa'))

    cursor=connection.cursor()
    cursor.execute("SELECT * from yayasan where no_sk_yayasan='"+nsk+"'")
    hasil=cursor.fetchone();
    if (hasil):
        if len(hasil)>0:
            messages.error(request, "Register Gagal: Nomor SK Yayasan "+nsk+" sudah pernah diregistrasi")
            return HttpResponseRedirect(reverse('account:register-mahasiswa'))

    values = "'"+username+"','"+password+"','yayasan'"
    query = "INSERT INTO pengguna (username,password,role) values (" + values + ")"
    cursor.execute(query)

    values = "'"+ni+"','"+email+"','"+nama+"','"+npwp+"','"+nomor_telepon+"','"+alamat+"','"+username+"'"
    query = "INSERT INTO donatur (nomor_identitas,email,nama,npwp,no_telp,alamat,username) values (" + values + ")"
    cursor.execute(query)

    values = "'"+nsk+"','"+email+"','"+nama+"','"+ni+"'"
    query = "INSERT INTO yayasan (no_sk_yayasan,email,nama,nomor_identitas_donatur) values (" + values + ")"
    cursor.execute(query)

    request.session['username']=username
    request.session['nama']=nama
    request.session['role']='yayasan'

    messages.success(request, "Register Berhasil: "+username)
    return HttpResponseRedirect(reverse('home'))
