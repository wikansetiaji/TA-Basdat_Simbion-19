from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^login/', login, name='login'),
    url(r'^register/', register, name='register'),
    url(r'^register_mahasiswa/', register_mahasiswa, name='register-mahasiswa'),
    url(r'^register_donatur/', register_donatur, name='register-donatur'),
    url(r'^post-register-mahasiswa/', post_mahasiswa, name='post-register-mahasiswa'),
    url(r'^post-register-donatur-individu/', post_individual_donor, name='post-register-donatur-individu'),
    url(r'^post-register-donatur-yayasan/', post_yayasan, name='post-register-donatur-yayasan'),
    url(r'^post-login/', post_login, name='post-login'),
]
