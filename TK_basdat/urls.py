"""TK_basdat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import login_register.urls as login_register
import daftar_beasiswa.urls as daftar_beasiswa
import acc_pendaftar_beasiswa.urls as acc_pendaftar
import register_beasiswa.urls as register_beasiswa
import info_beasiswa.urls as info_beasiswa
from login_register.views import home, logout


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', home, name="home"),
    url(r'^logout/', logout, name="logout"),
    url(r'^account/', include(login_register,namespace='account')),
    url(r'^mahasiswa/', include(daftar_beasiswa, namespace='regis_beasiswa')),
    url(r'^donatur/', include(acc_pendaftar, namespace='penerimaan')),
    url(r'^register_beasiswa/', include(register_beasiswa, namespace='register_beasiswa')),
    url(r'^info_beasiswa/', include(info_beasiswa, namespace='info_beasiswa')),
]
