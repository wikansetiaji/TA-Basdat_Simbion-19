from django.apps import AppConfig


class AccPendaftarBeasiswaConfig(AppConfig):
    name = 'acc_pendaftar_beasiswa'
