from django.db import models, connection


# Create your models here.
class DataCommand(models.Manager):

	def insert_data(self, data):
		base_sql = "INSERT INTO SKEMA_BEASISWA (kode, nama, jenis, deskripsi, nomor_identitas_donatur) values("
		sql_command = []
		kumpulan_data = []

		for tmp in data:
			sql_command.append("(%s)" % ','.join(tmp))
			kumpulan_data.extend(tmp)

		sql = '%s%s' % (base_sql, ', '.join(sql_command))

		curs = connection.cursor()
		curs.execute(sql, kumpulan_data)

class DataBeasiswa(models.Model):
	
	command = DataCommand()

