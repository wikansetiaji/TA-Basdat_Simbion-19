from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^daftar', register_beasiswa, name='register_beasiswa'),
    url(r'^tambah', register_beasiswa_tambah, name='register_beasiswa_tambah'),
    url(r'^message_post_daftar', message_post_daftar, name='message_post_daftar'),
    url(r'^message_post_tambah', message_post_tambah, name='message_post_tambah'),
]
