from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.db import connections
from .forms import *
from .models import *
import datetime
# Create your views here.
response = {}
kode_beasiswa = []
cursor=connection.cursor()
cursor.execute("SET search_path to SIMBION;")
nomor_identitas_donatur = 'GDI68-7723283'

def register_beasiswa(request):
    response['form'] = Message_Form_Daftar	
    return render(request, 'register_beasiswa.html', response)

def register_beasiswa_tambah(request):
    response['form2'] = Message_Form_Tambah
    return render(request, 'register_beasiswa_2.html', response)

def message_post_daftar(request):
    form = Message_Form_Daftar(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        if (cek_kode(request.POST['kode']) == True):
            print("tullll")
            kode = request.POST['kode'] 
            nama_paket_beasiswa = request.POST['nama_paket_beasiswa']
            jenis_paket_beasiswa = request.POST['jenis_paket_beasiswa']
            deskripsi = request.POST['deskripsi']
            syarat_beasiswa = request.POST['syarat_beasiswa'].split(";")
            #nomor_identitas_donatur = get_nomor_identitas_donatur()
            values = "'"+kode+"','"+nama_paket_beasiswa+"','"+jenis_paket_beasiswa+"','"+deskripsi+"','"+nomor_identitas_donatur+"'"
            sql = "insert into skema_beasiswa (kode, nama, jenis, deskripsi, nomor_identitas_donatur) values ("+values+")"
            cursor.execute(sql)
            insert_into_syarat(syarat_beasiswa, kode)
            response['status'] = "Done"
            return HttpResponseRedirect('/register_beasiswa/daftar/')
        else:
            print("salah :(")
            response['status'] = "Fail"
            return HttpResponseRedirect('/register_beasiswa/daftar/')
    else:
        response['status'] = "Fail"
        return HttpResponseRedirect('/register_beasiswa/daftar/')

def cek_kode(kode):
	cursor.execute("SELECT kode FROM SKEMA_BEASISWA WHERE kode=%s", [kode])
	hasil = cursor.fetchone()
	if (hasil is None):
		print('sabiduy')
		return True
	else:
		print(hasil)
		print('gagal')
		return False

def get_nomor_identitas_donatur(username):
	cursor.execute("SELECT d.nomor_identitas FROM DONATUR d, PENGGUNA p WHERE p.username = '"+username+"' AND p.username=d.username")
	hasil = cursor.fetchone()
	return hasil

def message_post_tambah(request):
    form = Message_Form_Tambah(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        if (cek_no_urut(request.POST['nomor_urut'], request.POST['kode_tambah']) == True):
            print("tullll")
            kode_tambah = request.POST['kode_tambah'] 
            nomor_urut = request.POST['nomor_urut']
            tanggal_mulai = request.POST['tanggal_mulai']
            tanggal_tutup = request.POST['tanggal_tutup']
            jumlah_pendaftar = str(0)
            print(tanggal_tutup)
            periode = str(date_period(tanggal_mulai,tanggal_tutup)) + "Hari"
            status = get_status_period(tanggal_mulai,tanggal_tutup)
            values = "'"+kode_tambah+"','"+nomor_urut+"','"+tanggal_mulai+"','"+tanggal_tutup+"','"+periode+"','"+status+"', '"+jumlah_pendaftar+"'"
            sql = "insert into skema_beasiswa_aktif (kode_skema_beasiswa, no_urut, tgl_mulai_pendaftaran, tgl_tutup_pendaftaran, periode_penerimaan, status, jumlah_pendaftar) values ("+values+")"
            cursor.execute(sql)
            response['status'] = "Done"
            return HttpResponseRedirect('/register_beasiswa/tambah/')
        else:
            print("salah :(")
            response['status'] = "Fail"
            return HttpResponseRedirect('/register_beasiswa/tambah/')
    else:
        response['status'] = "Fail"
        return HttpResponseRedirect('/register_beasiswa/tambah/')

def cek_no_urut(no_urut, kode):
	cursor.execute("SELECT no_urut FROM SKEMA_BEASISWA_AKTIF WHERE no_urut='"+no_urut+"' AND kode_skema_beasiswa='"+kode+"'")
	hasil = cursor.fetchone()
	if (hasil is None):
		print('sabiduy')
		return True
	else:
		print(hasil)
		print('gagal')
		return False

def date_period(date_mulai, date_tutup):
	date_mulai = date_mulai.split("-")
	date_tutup = date_tutup.split("-")
	year = int(date_tutup[0]) - int(date_mulai[0])
	month = int(date_tutup[1]) - int(date_mulai[1])
	day = int(date_tutup[2]) - int(date_mulai[2])
	period = 0
	if (day < 0):
		day += 30
	period += (year*365)
	period += (month*30)
	period += day
	return period

def insert_into_syarat(syarat, kode):
	for p in syarat:
		cursor.execute("insert into syarat_beasiswa (kode_beasiswa, syarat) values ('"+kode+"', '"+p+"');")


def get_status_period(date_mulai, date_tutup):
	status = "Tidak Aktif"
	now = str(datetime.datetime.now().date()).split("-")
	now = list(map(int, now))
	date_tutup = list(map(int, date_tutup.split("-")))
	date_mulai = list(map(int, date_mulai.split("-")))
	if ((date_mulai[0] <= now[0] <= date_tutup[0]) or (date_mulai[1] <= now[1] <= date_tutup[1]) or (date_mulai[2] <= now[2] <= date_tutup[2])):
		status = "AKtif"
	return status

