from django import forms
from django.db import connection
from django.contrib.admin.widgets import AdminDateWidget

jenis_beasiswa=[
		('prestasi', 'Beasiswa Prestasi Akademik'),
		('bantuan_belajar', 'Beasiswa Bantuan Belajar'),
		('tugas_akhir', 'Beasiswa Bantuan Tugas Akhir')
	]

cursor=connection.cursor()
nomor_identitas_donatur = 'GDI68-7723283'
cursor.execute("SET search_path to SIMBION;")
cursor.execute("SELECT kode FROM SKEMA_BEASISWA WHERE nomor_identitas_donatur='"+nomor_identitas_donatur+"';")
kode_beasiswa= []
for p in list(cursor.fetchall()):
    #p = p.replace("(", "").replace(",)", "")
    print(p[0])
    kode_beasiswa.append((str(p[0]), p[0]))


class Message_Form_Daftar(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan link',
    }
    attrs = {
        'class': 'form-control'
    }
    description_attrs = {
        'type': 'text',
        'rows': 2,
        'class': 'status-form-textarea form-control',
    }

    kode = forms.CharField(label='Kode', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    nama_paket_beasiswa = forms.CharField(label = 'Nama Paket Beasiswa', required=True, widget=forms.TextInput(attrs=attrs))
    jenis_paket_beasiswa =  forms.CharField(label='Jenis Paket Beasiswa', widget=forms.Select(choices=jenis_beasiswa, attrs=attrs))
    deskripsi = forms.CharField(label='Deskripsi', max_length=50,required=True, widget=forms.Textarea(attrs=description_attrs))
    syarat_beasiswa = forms.CharField(label = 'Syarat Beasiswa', initial='Tolong pisahkan syarat beasiswa dengan karakter ";"', required=True, widget=forms.TextInput(attrs=attrs))


class Message_Form_Tambah(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan link',
    }
    attrs = {
        'class': 'form-control'
    }

    date_attrs = {
        'type' : "date",        
    }

    kode_tambah = forms.CharField(label='Kode Beasiswa', widget=forms.Select(choices=kode_beasiswa, attrs=attrs))
    nomor_urut = forms.IntegerField(label='Nomor Urut', required=True, widget=forms.TextInput(attrs=attrs))
    tanggal_mulai = forms.DateField(label='Tanggal Mulai Pendaftaran',required=True, widget = AdminDateWidget(attrs = date_attrs))
    tanggal_tutup = forms.DateField(label='Tanggal Tutup Pendaftaran',required=True, widget = AdminDateWidget(attrs = date_attrs))
