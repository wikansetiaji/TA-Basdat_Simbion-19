from django.db import connection
from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from django.http import HttpResponseRedirect

# Create your views here.
def regis_beasiswa(request):
	response = {}
	cursor=connection.cursor()
	cursor.execute("SELECT kode_skema_beasiswa from skema_beasiswa_aktif order by kode_skema_beasiswa asc")
	hasil1 = cursor.fetchall()
	list_hasil = list(hasil1)
	counter = 0
	for a in list_hasil:
		list_hasil[counter] = str(a)[1:-2]
		counter += 1
	response['table_kode'] = list_hasil

	# response['npm']=request.session['npm']
	
	return render(request, 'pendaftaran_beasiswa.html', response)

# def daftar_beasiswa(request):
# 	print('masuk')
# 	kode_beasiswa = request.POST['kode_beasiswa']
# 	npm = request.session['npm']
# 	email = "dodo@gmail.com" #request.POST['email']
# 	ip = request.POST['ips']

# 	cursor=connection.cursor()
# 	cursor.execute("SELECT no_urut from pendaftaran order by no_urut desc")
# 	hasil = str(cursor.fetchone())[1:-2]
# 	urutan = int(hasil) + 1
	
# 	values = urutan + "," + kode_beasiswa + "," +  + "," + + "," 

# 	query = "INSERT INTO pendaftaran (no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_terima) values (" + values + ")"
# 	cursor.execute(query)

# 	response = {}
# 	return render(request, 'pendaftaran_beasiswa.html')

